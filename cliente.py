import socket
import select
import threading
import signal

"""
        /////////////////////////////////
        /                               /
        /   PYTHON VERSION 2.7.X        /
        /   AUTOR: ALIRIOX              /
        /   CHAT SENSILLO POR CONSOLA   /
        /   USANDO SOCKETS              /
        /                               /
        /////////////////////////////////
"""

class AlarmException(Exception):
    pass

def alarmHandler(signum, frame):
    raise AlarmException

def nonBlockingRawInput(prompt='', timeout=20):
    signal.signal(signal.SIGALRM, alarmHandler)
    signal.alarm(timeout)
    try:
        text = raw_input(prompt)
        signal.alarm(0)
        return text
    except AlarmException:
        pass
    signal.signal(signal.SIGALRM, signal.SIG_IGN)
    return None

if __name__ == '__main__':

    s = socket.socket()
    s.settimeout(1)
    s.connect(("localhost", 9999))

    while True:

        mensaje = None

        try:
            mensaje = s.recv(1024)
        except socket.timeout:
            pass

        if mensaje != None:
            print ("< "+mensaje)
            if mensaje == "servidor no disponible":
                break

        mensaje = nonBlockingRawInput("", timeout = 1)

        if mensaje != None:
            s.send(mensaje)
            if mensaje == "exit":
                break

    print ("connect close")
    s.close()